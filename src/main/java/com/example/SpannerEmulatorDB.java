package com.example;

import com.sun.istack.internal.NotNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SpannerEmulatorDB {

    private static Connection con = null;

    static {
        String project = "spanner-demo";
        String instanceId = "instance-1";
        String databaseId = "database-1";

        String url = "jdbc:cloudspanner://localhost:9010/projects/" + project +
                "/instances/" + instanceId + "/databases/" + databaseId + ";usePlainText=true";
        try {
            con = DriverManager.getConnection(url);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return con;
    }
}
