package com.example;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
class OutboxId implements Serializable {
    @Column(name="locator")
    private String locator;

    @Column(name="version")
    private String version;

}