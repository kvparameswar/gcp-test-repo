package com.example;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import com.google.gson.JsonObject;
import lombok.SneakyThrows;
import net.minidev.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyDynamicDataIntegrationWithDB extends ResponseDefinitionTransformer {
    static Connection con
            = SpannerEmulatorDB.getConnection();

    @SneakyThrows
    @Override
    public ResponseDefinition transform(Request request, ResponseDefinition responseDefinition, FileSource files, Parameters parameters) {
        System.out.println("I am inside extension make database & call spanner emulator here call here");
        ///CHECK THE REQUEST PARAMETER AND CALL DATABASE DIFFERENT TABLE BASED ON REQUEST PARAMETERS
        Map<String, List<String>> params = getQueryParams(request.getUrl());
        System.out.println("param==>"+params.get("config"));
        String resp = "";
        if("spanner".equalsIgnoreCase(params.get("config").get(0))){
            resp = retrieve();
        }else{
            resp = "Others";
        }

        return new ResponseDefinitionBuilder()
                .withHeader("MyHeader", "Transformed")
                .withStatus(200)
                .withBody(resp)
                .build();
    }

    private static Map<String, List<String>> getQueryParams(String url) {
        try {
            Map<String, List<String>> params = new HashMap<String, List<String>>();
            String[] urlParts = url.split("\\?");
            if (urlParts.length > 1) {
                String query = urlParts[1];
                for (String param : query.split("&")) {
                    String[] pair = param.split("=");
                    String key = URLDecoder.decode(pair[0], "UTF-8");
                    String value = "";
                    if (pair.length > 1) {
                        value = URLDecoder.decode(pair[1], "UTF-8");
                    }

                    List<String> values = params.get(key);
                    if (values == null) {
                        values = new ArrayList<String>();
                        params.put(key, values);
                    }
                    values.add(value);
                }
            }

            return params;
        } catch (UnsupportedEncodingException ex) {
            throw new AssertionError(ex);
        }
    }

    private int add() throws SQLException {
        String query
                = "insert into outbox(locator, "
                + "version,parent_locator,created,data,status,retry_count,updated,processing_time_millis) VALUES " +
                "(?, ?,?,?,?,?,?,?,?)";
        PreparedStatement ps
                = con.prepareStatement(query);
        ps.setString(1, "ABC002");
        ps.setInt(2, 1);
        ps.setString(3, null);
        ps.setString(4, null);
        ps.setString(5, "data2");
        ps.setInt(6, 0);
        ps.setInt(7, 0);
        ps.setString(8, null);
        ps.setInt(9, 0);
        int n = ps.executeUpdate();
        return n;
    }

    private String retrieve() throws SQLException {
        String query = "SELECT * from OUTBOX";
        PreparedStatement ps
                = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        String resp = "";
        while (rs.next()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("locator",rs.getString("locator"));
            jsonObject.put("version",rs.getInt("version"));
            jsonObject.put("parent_locator",rs.getString("parent_locator"));
            jsonObject.put("created",rs.getString("created"));
            jsonObject.put("data",rs.getString("data"));
            jsonObject.put("status",rs.getInt("status"));
            jsonObject.put("retry_count",rs.getInt("retry_count"));
            jsonObject.put("updated",rs.getString("updated"));
            jsonObject.put("processing_time_millis",rs.getInt("processing_time_millis"));
            resp = jsonObject.toString();
//            System.out.println("locator ==>" + rs.getString("locator"));
//            System.out.println("version ==>" + rs.getInt("version"));
//            System.out.println("parent_locator ==>" + rs.getString("parent_locator"));
//            System.out.println("created ==>" + rs.getString("created"));
//            System.out.println("data ==>" + rs.getString("data"));
//            System.out.println("status ==>" + rs.getInt("status"));
//            System.out.println("retry_count ==>" + rs.getInt("retry_count"));
//            System.out.println("updated ==>" + rs.getString("updated"));
//            System.out.println("processing_time_millis ==>" + rs.getInt("processing_time_millis"));

        }
        return resp;
    }

    public String getName() {
        return "example";
    }
}
