package com.example;

import lombok.Data;
import org.springframework.cloud.gcp.data.spanner.core.mapping.Column;
import org.springframework.cloud.gcp.data.spanner.core.mapping.PrimaryKey;
import org.springframework.cloud.gcp.data.spanner.core.mapping.Table;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;


@Data
@Entity
@Table(name = "OUTBOX")
public class Outbox {

    @EmbeddedId
    private OutboxId outboxId;

    @Column(name="parent_locator")
    private String parentLoc;

    @Column(name="created")
    private Date created;

    @Column(name="data")
    private String data;

    @Column(name="status")
    private Integer status;

    @Column(name="retry_count")
    private Integer retryCount;

    @Column(name="updated")
    private Date updated;
}
