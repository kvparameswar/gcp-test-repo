package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OutboxController {

    @Autowired
    TestService testService;

    @GetMapping
    public List<Outbox> findAll() {
        testService.getDetails();
        return null;
    }

    @GetMapping("/stub")
    public String stub() {
        return "{\"parent_locator\":\"\",\"data\":\"data\",\"created\":null,\"retry_count\":0,\"processing_time_millis\":0,\"locator\":\"ABC001\",\"version\":1,\"updated\":null,\"status\":0}";
    }
}
