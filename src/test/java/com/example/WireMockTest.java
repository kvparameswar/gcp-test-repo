package com.example;

import com.github.tomakehurst.wiremock.WireMockServer;
import lombok.SneakyThrows;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.Assert.assertEquals;

public class WireMockTest {

//    static Connection con
//            = SpannerEmulatorDB.getConnection();

    @SneakyThrows
    @Test
    public void testWireMock(){
        WireMockServer wireMockServer = new WireMockServer(
                wireMockConfig().extensions(MyDynamicDataIntegrationWithDB.class)
        );
        wireMockServer.start();
        //configureFor("localhost", 8080);
//        int n = add();
       // retrieve();
        /*Iterable<Outbox> list;*/
        //stubFor(get(urlEqualTo("/api/stub")).willReturn(aResponse().withBody("stubbed")));

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet("http://localhost:8080");
        String reqPara = "Spanner";
        URI uri = new URIBuilder(request.getURI())
                .addParameter("config", reqPara)
                .build();
        ((HttpRequestBase) request).setURI(uri);
        HttpResponse httpResponse = httpClient.execute(request);
        String responseString = convertResponseToString(httpResponse);

        System.out.println("*****====>"+responseString);
//        verify(getRequestedFor(urlEqualTo("/api/stub")));
        //assertEquals("stubbed", responseString);

        wireMockServer.stop();
    }

//    private int add() throws SQLException {
//        String query
//                = "insert into outbox(locator, "
//                + "version,parent_locator,created,data,status,retry_count,updated,processing_time_millis) VALUES " +
//                "(?, ?,?,?,?,?,?,?,?)";
//        PreparedStatement ps
//                = con.prepareStatement(query);
//        ps.setString(1, "ABC002");
//        ps.setInt(2, 1);
//        ps.setString(3, null);
//        ps.setString(4, null);
//        ps.setString(5, "data2");
//        ps.setInt(6, 0);
//        ps.setInt(7, 0);
//        ps.setString(8, null);
//        ps.setInt(9, 0);
//        int n = ps.executeUpdate();
//        return n;
//    }
//
//    private void retrieve() throws SQLException {
//        String query = "SELECT * from OUTBOX";
//        PreparedStatement ps
//                = con.prepareStatement(query);
//        ResultSet rs = ps.executeQuery();
//
//        while (rs.next()) {
//            System.out.println("locator ==>" + rs.getString("locator"));
//            System.out.println("version ==>" + rs.getInt("version"));
//            System.out.println("parent_locator ==>" + rs.getString("parent_locator"));
//            System.out.println("created ==>" + rs.getString("created"));
//            System.out.println("data ==>" + rs.getString("data"));
//            System.out.println("status ==>" + rs.getInt("status"));
//            System.out.println("retry_count ==>" + rs.getInt("retry_count"));
//            System.out.println("updated ==>" + rs.getString("updated"));
//            System.out.println("processing_time_millis ==>" + rs.getInt("processing_time_millis"));
//        }
//    }

    private String convertResponseToString(HttpResponse response) throws IOException {
        InputStream responseStream = response.getEntity().getContent();
        Scanner scanner = new Scanner(responseStream, "UTF-8");
        String responseString = scanner.useDelimiter("\\Z").next();
        scanner.close();
        return responseString;
    }



}
